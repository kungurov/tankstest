﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Types;
using UnityEngine.Networking.Match;

public class LobbyManager : NetworkLobbyManager
{
    public delegate void OnGameCreatedDelegate(bool success);
    public event OnGameCreatedDelegate OnGameCreated = delegate { };

    static public LobbyManager Instance;

    [HideInInspector]
    public bool IsMatchMaking = false;

    [HideInInspector]
    public bool IsDisconnectServer = false;

    private ulong _currentMatchID;

    private LobbyHook _lobbyHooks;

    private Dictionary<int, LobbyPlayer> _lobbyPlayers;

    public void Start()
    {
        Instance = this;
        UIManager.sharedInstance.InitData();
        UIManager.sharedInstance.InitializationRoot(ID_POPUP.MAIN_MENU);
        _lobbyHooks = GetComponent<TankLobbyHook>();
        _lobbyPlayers = new Dictionary<int, LobbyPlayer>();
    }

    public void CreateGame(string serverName)
    {
        StartMatchMaker();
        matchMaker.CreateMatch(
            serverName,
            (uint)maxPlayers,
            true,
            "",
            "",
            "",
            0,
            0,
            OnMatchCreate);
        IsMatchMaking = true;
    }

    public override void OnMatchCreate(bool success, string extendedInfo, MatchInfo matchInfo)
    {
        base.OnMatchCreate(success, extendedInfo, matchInfo);
        _currentMatchID = (System.UInt64)matchInfo.networkId;
        OnGameCreated(success);
    }

    public void StopHostProcess()
    {
        if (IsMatchMaking)
        {
            matchMaker.DestroyMatch((NetworkID)_currentMatchID, 0, OnDestroyMatch);
            IsDisconnectServer = true;
            IsMatchMaking = false;
        }
        else
        {
            StopHost();
        }
    }

    public override void OnDestroyMatch(bool success, string extendedInfo)
    {
        base.OnDestroyMatch(success, extendedInfo);
        if (IsDisconnectServer)
        {
            StopMatchMaker();
            StopHost();
        }
    }

    public void JoinPlayer(NetworkID netId)
    {
        matchMaker.JoinMatch(netId, "", "", "", 0, 0, OnMatchJoined);
        UIManager.sharedInstance.ShowLoadingPanel();
    }

    public override void OnMatchJoined(bool success, string extendedInfo, MatchInfo matchInfo)
    {
        base.OnMatchJoined(success, extendedInfo, matchInfo);
        UIManager.sharedInstance.HideLoadingPanel();
    }

    public override GameObject OnLobbyServerCreateLobbyPlayer(NetworkConnection connection, short playerControllerId)
    {
        return CreateLobbyPlayerObject(connection.connectionId);
    }

    public GameObject CreateLobbyPlayerObject(int id)
    {
        GameObject lobbyPlayerObject = Instantiate(lobbyPlayerPrefab.gameObject) as GameObject;
        lobbyPlayerObject.transform.parent = transform;
        LobbyPlayer lobbyPlayer = lobbyPlayerObject.GetComponent<LobbyPlayer>();
        if (lobbyPlayer != null)
        {
            lobbyPlayer.ID = id;
            AddLobbyPlayer(lobbyPlayer);
        }
        return lobbyPlayerObject;
    }

    private void AddLobbyPlayer(LobbyPlayer lobbyPlayer)
    {
        if (_lobbyPlayers.ContainsKey(lobbyPlayer.ID))
        {
            _lobbyPlayers.Add(lobbyPlayer.ID, lobbyPlayer);
        }
    }

    private void RemoveLobbyPlayer(int id)
    {
        if (_lobbyPlayers.ContainsKey(id))
        {
            _lobbyPlayers.Remove(id);
        }
    }

    public override void OnLobbyServerDisconnect(NetworkConnection connection)
    {
        RemoveLobbyPlayer(connection.connectionId);
    }

    public override void OnLobbyServerPlayerRemoved(NetworkConnection connection, short playerControllerId)
    {
        RemoveLobbyPlayer(connection.connectionId);
    }

    public override void OnLobbyServerPlayersReady()
    {
        ServerChangeScene(playScene);
    }

    public override bool OnLobbyServerSceneLoadedForPlayer(GameObject lobbyPlayer, GameObject gamePlayer)
    {
        if (_lobbyHooks)
        {
            _lobbyHooks.OnLobbyServerSceneLoadedForPlayer(this, lobbyPlayer, gamePlayer);
        }
        return true;
    }
}
