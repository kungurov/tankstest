﻿using UnityEngine.Networking;

public class LobbyPlayer : NetworkLobbyPlayer
{
    private LobbyPlayerView lobbyPlayerView = null;

    [SyncVar]
    public int ID;

    [SyncVar(hook = "OnSetName")]
    public string playerName = "";

	[SyncVar(hook = "OnSetTeam")]
	public TEAM_TANK Team;

    public void CreatePlayerView()
    {
        ViewControllerBase view = UIManager.sharedInstance.GetTopView();
        if (view.ID == ID_POPUP.LOBBY_MENU)
        {
            LobbyViewController lobbyView = view as LobbyViewController;
            if (lobbyView != null)
            {
                lobbyPlayerView = lobbyView.CreateLobbyPlayerView();
                lobbyPlayerView.Initialzation();
                lobbyPlayerView.OnPlayerReady += SendReadyToBeginMessage;
            }
        }
    }

    public bool DestroyPlayerView()
    {
        if (UIManager.sharedInstance != null)
        {
            ViewControllerBase view = UIManager.sharedInstance.GetTopView();
            if (view.ID == ID_POPUP.LOBBY_MENU)
            {
                LobbyViewController lobbyView = view as LobbyViewController;
                if (lobbyView != null)
                {
                    if (lobbyPlayerView != null)
                    {
                        lobbyView.DestroyLobbyPlayerView(lobbyPlayerView);
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public void OnDestroy()
    {
        DestroyPlayerView();
    }

    public override void OnClientEnterLobby()
    {
        base.OnClientEnterLobby();

        CreatePlayerView();

        if (isLocalPlayer)
        {
            SetLocalPlayer();
        }
        else
        {
            SetupOtherPlayer();
        }
        OnSetName(playerName);
		OnSetTeam(Team);
    }

    public override void OnStartAuthority()
    {
        base.OnStartAuthority();
        SetLocalPlayer();
    }

    void SetLocalPlayer()
    {
        if (playerName == "")
        {
            CmdNameChanged("Player_" + ID);
        }
		if (Team == TEAM_TANK.NONE)
		{
			CmdTeamChanged(ID % 2 == 0 ? TEAM_TANK.RED : TEAM_TANK.BLUE);
		}
        if (lobbyPlayerView != null)
        {
            lobbyPlayerView.SetJoinStatus();
        }
    }

    void SetupOtherPlayer()
    {
        OnClientReady(readyToBegin);
    }

    public override void OnClientReady(bool readyState)
    {
        if (readyState)
        {
            if (lobbyPlayerView != null)
            {
                lobbyPlayerView.SetReadyStatus();
            }
        }
        else
        {
            if (lobbyPlayerView != null)
            {
                lobbyPlayerView.SetWaitingStatus();
            }
        }
    }

    public void OnSetName(string name)
    {
        playerName = name;
        if (lobbyPlayerView != null)
        {
            lobbyPlayerView.SetPlayerName(playerName);
        }
    }

	public void OnSetTeam(TEAM_TANK team)
	{
		Team = team;
		if (lobbyPlayerView != null)
		{
			lobbyPlayerView.SetPlayerTeam(Team);
		}
	}

    [Command]
    public void CmdNameChanged(string name)
    {
        playerName = name;
    }

	[Command]
	public void CmdTeamChanged(TEAM_TANK team)
	{
		Team = team;
	}
}
