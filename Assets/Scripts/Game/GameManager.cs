﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;

public enum END_GAME_STATUS
{	
	NONE,
	TIME_OUT,
	RED_WIN,
	BLUE_WIN
}

public class GameManager : NetworkBehaviour
{
    static public GameManager Instance;

    public CameraFollow gameCamera;

    public static List<TankGame> _tanks = new List<TankGame>();

    public BaseGame baseRed;
    public BaseGame baseBlue;

	[SyncVar(hook = "OnEndStatusChanged")]    
	public END_GAME_STATUS EndStatus = END_GAME_STATUS.NONE;

	public float GameTime = 300;
	public GameTimer timer;

	private GameMenuViewController gameView;

	void Awake()
    { 
        Instance = this;
		timer.OnTimeOut += () => { SetEndStatus(END_GAME_STATUS.TIME_OUT); };
    }

    private void Start()
    {
        SetCameraFollow();
		SetTankTeamColor();
		SpawnTanks();
		ShowGameUI();
		StartGameTimer();
    }

	[Server]
	public void StartGameTimer()
	{
		if (isServer) 
		{
			timer.StartTimer(GameTime);
		}
	}

	public void Update()
	{
		gameView.SetTimeLeft(timer.GetTimeLeft());
	}

	public void ShowGameUI()
	{
		gameView =  UIManager.PushView(ID_POPUP.GAME_MENU) as GameMenuViewController;
		baseRed.SetView(gameView);
		baseBlue.SetView(gameView);
	}

    public void SetCameraFollow()
    {
        foreach (TankGame tank in _tanks)
        {
            if (tank.isLocalPlayer)
            {
                gameCamera.SetTarget(tank.transform);
            }
        }
    }

    public void SetTankTeamColor()
    {
        foreach (TankGame tank in _tanks)
        {
            tank.SetTeamColor();
            switch (tank.Team)
            {
                case TEAM_TANK.RED:
                    tank.BaseGame = baseRed;
                    break;
                case TEAM_TANK.BLUE:
                    tank.BaseGame = baseBlue;
                    break;
            }
        }
    }

	public void SpawnTanks()
    {
        foreach (TankGame tank in _tanks)
        {
			tank.Respawn();
        }
    }

    public static void AddTank(GameObject tankObject, int id, string name, TEAM_TANK team)
    {
        TankGame tank = tankObject.GetComponent<TankGame>();
		tank.ID = id;
		tank.Name = name;
		tank.Team = team;
        _tanks.Add(tank);
    }

	[Server]
	public void SetEndStatus(END_GAME_STATUS status)
	{
		EndStatus = status;
	}

	public void OnEndStatusChanged(END_GAME_STATUS status)
	{
		EndStatus = status;
		ShowEndGameStatus();
	}

	public void ShowEndGameStatus()
	{
		EndPopupViewController popup = UIManager.PushViewPopUp(ID_POPUP.END_POPUP) as EndPopupViewController;
		if (popup != null) 
		{
			popup.ShowEndGameStatus(EndStatus);	
		}
	}
}
