﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class BaseGame : NetworkBehaviour, IDamaging
{
	private GameMenuViewController gameView;

    public Transform SpawnPoint;
    public MeshRenderer meshMain;

    public const int MaxHeaith = 20;

    [SyncVar]
    public TEAM_TANK Team;

	[SyncVar(hook = "OnHealthChanged")]    
	public int CurrentHealth = MaxHeaith;

    public void Start()
    {
        SetTeamColor();
    }

	public void SetView(GameMenuViewController view)
	{
		gameView = view;
		UpdateHealth();
	}

    public void SetTeamColor()
    {
        switch (Team)
        {
            case TEAM_TANK.RED:
                meshMain.material.color = Color.red;
                break;
            case TEAM_TANK.BLUE:
                meshMain.material.color = Color.blue;
                break;
        }
    }

	[Server]
	public void Damage()
	{
		CurrentHealth = CurrentHealth - 1;

		if (CurrentHealth <= 0)
		{
			switch (Team) 
			{
			case TEAM_TANK.RED:
				GameManager.Instance.SetEndStatus(END_GAME_STATUS.BLUE_WIN);
				break;
			case TEAM_TANK.BLUE:
				GameManager.Instance.SetEndStatus(END_GAME_STATUS.RED_WIN);
				break;
			}
		}
	}

	void OnHealthChanged(int health)
	{
		CurrentHealth = health;
		UpdateHealth();
	}	

	public void UpdateHealth()
	{
		if (gameView != null) 
		{
			gameView.SetScore(Team, CurrentHealth);
		}
	}
}
