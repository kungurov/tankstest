﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public float cameraDistance = 16.0f;
    public float cameraHeight = 10.0f;

    private Vector3 cameraOffset;

    private Transform target = null;

    public void SetTarget(Transform follow)
    {
        cameraOffset = new Vector3(0f, cameraHeight, -cameraDistance);
        target = follow;
    }

    void MoveCamera()
    {
        if (target != null)
        {
            transform.position = target.transform.position;
            transform.rotation = target.transform.rotation;
            transform.Translate(cameraOffset);
            transform.LookAt(target.transform);
        }
    }

    void Update()
    {
        MoveCamera();
    }
}
