﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;

public class GameTimer : NetworkBehaviour 
{
	public event Action OnTimeOut = delegate { };

	private bool _active = false;

	[SyncVar]
	private float _time;

	public void StartTimer(float time)
	{
		_active = true;
		_time = time;
	}

	public void StopTimer()
	{
		_active = false;
	}

	public TimeSpan GetTimeLeft()
	{
		return TimeSpan.FromSeconds(_time);
	}

	public void Update()
	{
		if (_active) 
		{
			_time -= Time.deltaTime;
			if (_time <= 0) 
			{
				StopTimer();
				OnTimeOut();
			}
		}	
	}
}
