﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public enum TEAM_TANK
{
	NONE,
	RED,
	BLUE
}

public class TankGame : NetworkBehaviour, IDamaging
{
    [SerializeField]
    private GunScript currentGun;

    [SerializeField]
    private TankController tankController;

	[SyncVar]
	public int ID;

	[SyncVar]
	public string Name;

	[SyncVar]
	public TEAM_TANK Team;

    public BaseGame BaseGame { get; set; }

	public MeshRenderer meshMain;
	public MeshRenderer meshGun;

    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();
		meshGun.material.color = Color.green;
    }

	public void SetTeamColor()
	{
		currentGun.SetTeam(Team);
		switch (Team) 
		{
		case TEAM_TANK.RED:
			meshMain.material.color = Color.red;
			break;
		case TEAM_TANK.BLUE:
			meshMain.material.color = Color.blue;
			break;
		}
	}

    public override void OnStartClient()
    {
        if (!isServer)
        {
			GameManager.AddTank(gameObject, ID, Name, Team);
        }
    }

    void Update()
	{
		if (!isLocalPlayer)
		{
			return;
		}
		if (GameManager.Instance.EndStatus == END_GAME_STATUS.NONE)
		{
	        tankController.OnUpdate();

	        if (Input.GetKeyDown(KeyCode.Space))
			{
	            if (currentGun.IsCanShoot())
	            {
	                currentGun.Shoot();
	            }
	        }
		}
    }

    private void FixedUpdate()
    {
        tankController.OnFixedUpdate();
    }

    public void Respawn()
    {
		if (isLocalPlayer) 
		{
			tankController.SetSpawnPosition(BaseGame.SpawnPoint.position, BaseGame.SpawnPoint.rotation);
		}
    }

	public void Damage()
	{
		RpcDamage();
	}

    [ClientRpc]
    public void RpcDamage()
    {
        if (isLocalPlayer)
        {
            Respawn();
        }
    }
}
