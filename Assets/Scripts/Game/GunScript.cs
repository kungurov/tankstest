﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public enum GUN_STATUS
{
    READY,
    RELOADING
}

public class GunScript : NetworkBehaviour, IShooting
{
    public GameObject gunObject;
    public Transform bulletSpawn;
    public GameObject bulletPrefab;
	[SyncVar]
    private GUN_STATUS _status = GUN_STATUS.READY;
    public float timeReloading = 1.0f;

    private TEAM_TANK team = TEAM_TANK.NONE;

	public void SetTeam(TEAM_TANK team)
	{
		this.team = team;
	}

	public void Shoot()
    {
		CmdShoot(bulletSpawn.position, bulletSpawn.rotation, team);
    }

    [Command]
    void CmdShoot(Vector3 position, Quaternion rotation, TEAM_TANK team)
    {
        BulletScript bullet = Instantiate(bulletPrefab).GetComponent<BulletScript>();
        if (bullet != null)
        {
			StartCoroutine(SetReloading());
			bullet.Fire(position, rotation, team);
            NetworkServer.Spawn(bullet.gameObject);
        }
    }

	public IEnumerator SetReloading()
	{
		_status = GUN_STATUS.RELOADING;
		yield return new WaitForSeconds(timeReloading);
		_status = GUN_STATUS.READY;
	}

    public bool IsCanShoot()
    {
        return (_status == GUN_STATUS.READY);
    }
}
