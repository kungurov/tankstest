﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TankLobbyHook : LobbyHook
{
    public override void OnLobbyServerSceneLoadedForPlayer(NetworkManager manager, GameObject lobbyPlayerObject, GameObject gamePlayer)
    {
		if (lobbyPlayerObject != null) 
		{
			LobbyPlayer lobbyPlayer = lobbyPlayerObject.GetComponent<LobbyPlayer>();
			if (lobbyPlayer != null) 
			{
				GameManager.AddTank(gamePlayer, lobbyPlayer.ID, lobbyPlayer.playerName, lobbyPlayer.Team);
			}
		}
    }
}
