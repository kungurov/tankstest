﻿public interface IShooting
{
    void Shoot();
}

public interface IDamaging
{
    void Damage(); 
}