﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class BulletScript : NetworkBehaviour
{
    private Rigidbody _bulletRigidbody;

	public float MoveSpeed = 20.0f;
	public float MoveTime = 3.0f;
    
    [SyncVar]
    private TEAM_TANK team = TEAM_TANK.NONE;
		
    void Awake()
    {
        _bulletRigidbody = GetComponent<Rigidbody>();
    }

	public void Fire(Vector3 position, Quaternion rotation, TEAM_TANK team)
    {
		this.team = team;
        transform.position = position;
        transform.rotation = rotation;
        _bulletRigidbody.velocity = transform.forward * MoveSpeed;
        Destroy(gameObject, MoveTime);
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (!isServer)
        {
            return;
        }
        TankGame tank = collision.gameObject.GetComponent<TankGame>();
        if (tank != null)
        {
			if (tank.Team != team) 
			{
                tank.Damage();
            }
        }
        BaseGame baseTanks = collision.gameObject.GetComponent<BaseGame>();
        if (baseTanks != null)
        {
            if (baseTanks.Team != team)
            {
				baseTanks.Damage();
            }
        }
        CmdDestroyBullet();
    }

    [Command]
    public void CmdDestroyBullet()
    {
        Destroy(gameObject);
    }
}
