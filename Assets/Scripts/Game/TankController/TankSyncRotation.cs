﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TankSyncRotation : NetworkBehaviour 
{
	[SerializeField] 
	private Transform _transform;

	[SyncVar] 
	private Quaternion syncRotation;

	[SerializeField] 
	public float rotationLerpRate = 15;

	private Quaternion lastRotation;
	private float threshold = 5f;

	private void FixedUpdate() 
	{
		LerpRotation();
		TransmitRotation(); 
	}

	private void LerpRotation() 
	{
		if (!isLocalPlayer) 
		{
			_transform.rotation = Quaternion.Lerp(_transform.rotation, syncRotation, Time.deltaTime * rotationLerpRate);
		}
	}

	[Command]
	private void CmdRotationToServer(Quaternion objectRotation) 
	{
		syncRotation = objectRotation;
	}

	[ClientCallback]
	private void TransmitRotation() 
	{
		if (isLocalPlayer && Quaternion.Angle(_transform.rotation, lastRotation) > threshold) 
		{
			CmdRotationToServer(_transform.rotation);
			lastRotation = _transform.rotation;
		}
	}

}
