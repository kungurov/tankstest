﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TankSyncPosition : NetworkBehaviour 
{
	[SerializeField] 
	private Transform _transform;

	[SyncVar] 
	private Vector3 syncPosition;

	[SerializeField] 
	private float positionLerpRate = 15;

	private Vector3 lastPosition;
	private float threshold = 0.5f;

	void FixedUpdate() 
	{
		TransmitPosition();
		LerpPosition();
	}

	void LerpPosition() 
	{
		if (!isLocalPlayer) 
		{
			_transform.position = Vector3.Lerp(_transform.position, syncPosition, Time.deltaTime * positionLerpRate);
		}
	}

	[Command]
	void CmdPositionToServer(Vector3 pos) 
	{
		syncPosition = pos;
	}

	[ClientCallback]
	void TransmitPosition() 
	{
		if (isLocalPlayer && Vector3.Distance(_transform.position, lastPosition) > threshold) 
		{
			CmdPositionToServer(_transform.position);
			lastPosition = _transform.position;
		}
	}
}
