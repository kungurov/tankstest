﻿using UnityEngine;
using UnityEngine.Networking;

public class TankController : NetworkBehaviour
{
    public float moveSpeed = 8f;
    public float turnSpeed = 80f;

    [SerializeField]
    private Rigidbody _rigidbody;

    private float moveInput;
    private float turnInput;

    public void OnUpdate()
    {
        moveInput = Input.GetAxis("Vertical");
        turnInput = Input.GetAxis("Horizontal");
    }

    public void OnFixedUpdate()
    {
        if (!isLocalPlayer)
        {
            return;
        }

        Move();
        Turn();
    }

    private void Move()
    {
        Vector3 movement = transform.forward * moveInput * moveSpeed * Time.deltaTime;
        _rigidbody.MovePosition(_rigidbody.position + movement);
    }


    private void Turn()
    {
        float turn = turnInput * turnSpeed * Time.deltaTime;
        Quaternion inputRotation = Quaternion.Euler(0f, turn, 0f);
        _rigidbody.MoveRotation(_rigidbody.rotation * inputRotation);
    }

	public void SetSpawnPosition(Vector3 position, Quaternion rotation)
	{
        _rigidbody.position = position;
        _rigidbody.rotation = rotation;
    }
}
