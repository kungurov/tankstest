﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MainMenuViewController : ViewControllerBase
{
    private LobbyManager lobbyManager;

    public UIButton btnCreateGame;
    public UIButton btnFindServers;
    public UIInput inputServerName;

    public override void onStart()
    {
        lobbyManager = LobbyManager.Instance;
    }

    public override void viewWillAppear()
    {
        base.viewWillAppear();
        btnCreateGame.isEnabled = true;
        btnFindServers.isEnabled = true;
        inputServerName.enabled = true;

        UIEventListener.Get(btnCreateGame.gameObject).onClick += OnCreateGameClick;
        UIEventListener.Get(btnFindServers.gameObject).onClick += OnFindServersClick;
    }

    public override void viewWillDisAppear()
    {
        base.viewWillDisAppear();
        btnCreateGame.isEnabled = false;
        btnFindServers.isEnabled = false;
        inputServerName.enabled = false;

        UIEventListener.Get(btnCreateGame.gameObject).onClick -= OnCreateGameClick;
        UIEventListener.Get(btnFindServers.gameObject).onClick -= OnFindServersClick;
    }

    void OnCreateGameClick(GameObject sender)
    {
        string serverName = inputServerName.value;
        if (!string.IsNullOrEmpty(serverName))
        {
            UIManager.sharedInstance.ShowLoadingPanel();
            lobbyManager.CreateGame(serverName);
            lobbyManager.OnGameCreated += OnGameCreated;
        }
    }

    void OnFindServersClick(GameObject sender)
    {
        UIManager.PushView(ID_POPUP.FIND_SERVERS_MENU);
    }

    void OnGameCreated(bool success)
    {
        UIManager.sharedInstance.HideLoadingPanel();
        lobbyManager.OnGameCreated -= OnGameCreated;
        if (success)
        {
            UIManager.PushView(ID_POPUP.LOBBY_MENU);
        }
    }
}
