﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameMenuViewController : ViewControllerBase
{
	[SerializeField]
	private UILabel labelTime;

	[SerializeField]
	private UILabel labelRedTeamScore;

	[SerializeField]
	private UILabel labelBlueTeamScore;

	public void SetScore(TEAM_TANK team, int value)
	{
		switch (team) 
		{
		case TEAM_TANK.RED:
			labelRedTeamScore.text = value.ToString();
			break;
		case TEAM_TANK.BLUE:
			labelBlueTeamScore.text = value.ToString();
			break;
		}
	}

	public void SetTimeLeft(TimeSpan time)
	{
		labelTime.text = string.Format("{0}:{1:00}", time.Minutes, time.Seconds);
	}
}
