﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking.Match;

public class ServerItemView : MonoBehaviour
{
    private LobbyManager lobbyManager;
    private MatchInfoSnapshot _matchInfo;

    public UILabel labelServerName;
    public UILabel labelUsersNumber;
    public UIButton btnJoin;

    public void SetServerData(MatchInfoSnapshot data)
    {
        lobbyManager = LobbyManager.Instance;
        if (data != null)
        {
            _matchInfo = data;
            labelServerName.text = _matchInfo.name;
            labelUsersNumber.text = _matchInfo.currentSize.ToString() + "/" + _matchInfo.maxSize.ToString();
            SetJoinButton();
        }
    }

    private void SetJoinButton()
    {
        if (_matchInfo.currentSize < _matchInfo.maxSize)
        {
            UIEventListener.Get(btnJoin.gameObject).onClick = OnJoinClick;
        }
        else
        {
            btnJoin.enabled = false;
        }
    }

    void OnJoinClick(GameObject sender)
    {
        lobbyManager.JoinPlayer(_matchInfo.networkId);
        UIManager.PushView(ID_POPUP.LOBBY_MENU);
    }
}
