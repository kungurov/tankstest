﻿using UnityEngine;
using System;

public class LobbyPlayerView : MonoBehaviour
{
    public event Action OnPlayerReady = delegate { };

    [SerializeField]
    private UILabel labelPlayerName;

	[SerializeField]
	private UILabel labelTeam;

    [SerializeField]
    private UILabel labelButtonStatus;

    [SerializeField]
    private UIButton btnReady;

    public void Initialzation()
    {
        UIEventListener.Get(btnReady.gameObject).onClick = OnReadyButtonClick;
        btnReady.isEnabled = true;
    }

    private void OnReadyButtonClick(GameObject sender)
    {
        OnPlayerReady();
    }

    public void SetPlayerName(string name)
    {
        labelPlayerName.text = name;
    }

	public void SetPlayerTeam(TEAM_TANK team)
	{
		labelTeam.text = team.ToString();
		switch (team) 
		{
		case TEAM_TANK.RED:
			labelTeam.color = Color.red;
			break;
		case TEAM_TANK.BLUE:
			labelTeam.color = Color.blue;
			break;
		}
	}

    public void SetReadyStatus()
    {
        labelButtonStatus.text = "Готово";
        btnReady.isEnabled = false;
    }

    public void SetWaitingStatus()
    {
        labelButtonStatus.text = "...";
        btnReady.isEnabled = false;
    }

    public void SetJoinStatus()
    {
        labelButtonStatus.text = "Подвердить";
        btnReady.isEnabled = true;
    }
}
