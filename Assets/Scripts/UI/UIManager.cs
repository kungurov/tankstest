﻿using UnityEngine;
using System.Collections.Generic;

public enum ID_POPUP
{
    NOT,
    MAIN_MENU,
    FIND_SERVERS_MENU,
    LOBBY_MENU,
    GAME_MENU,
	END_POPUP
}

public class UIManager : Singleton<UIManager>
{
    public UIRoot uiRoot;

    public UIPanel loadingPanel;

    public GameObject mainMenu;
    public GameObject lobbyMenu;
    public GameObject findServersMenu;
    public GameObject gameMenu;
	public GameObject endPopup;

    private Dictionary<ID_POPUP, GameObject> UItems;

    public void InitData()
    {
        UItems = new Dictionary<ID_POPUP, GameObject>();
        UItems.Add(ID_POPUP.MAIN_MENU, mainMenu);
        UItems.Add(ID_POPUP.LOBBY_MENU, lobbyMenu);
        UItems.Add(ID_POPUP.FIND_SERVERS_MENU, findServersMenu);
        UItems.Add(ID_POPUP.GAME_MENU, gameMenu);
		UItems.Add(ID_POPUP.END_POPUP, endPopup);
		DontDestroyOnLoad(gameObject);
    }

    private Stack<ViewControllerBase> viewControllersStack;
    private ViewControllerBase rootViewController;
    private ViewControllerBase topViewController;

    public ViewControllerBase InitializationRoot(ID_POPUP item)
    {
        viewControllersStack = new Stack<ViewControllerBase>();
        rootViewController = InstantiateObject(item);
        SetViewControllerVisible(rootViewController, true);
        return rootViewController;
    }

    private ViewControllerBase InstantiateObject(ID_POPUP item)
    {
        GameObject uiElement = NGUITools.AddChild(uiRoot.gameObject, UItems[item]);
        ViewControllerBase viewElement = uiElement.GetComponent<ViewControllerBase>();
        viewElement.onStart();
        viewElement.ID = item;
        viewElement._uiRoot = uiRoot;
        viewElement._viewObject = uiElement;
        return viewElement;
    }

    protected ViewControllerBase PopViewControllerLocal()
    {
        if (viewControllersStack.Count != 0)
        {
            return viewControllersStack.Pop();
        }
        return null;
    }

    public ViewControllerBase PeekViewController()
    {
        return viewControllersStack.Peek();
    }

    public static ViewControllerBase PopView()
    {
        return UIManager.sharedInstance.PopViewController();
    }

    private ViewControllerBase PopViewController()
    {
        if (viewControllersStack.Count > 0)
        {
            SetViewControllerVisible(topViewController, false);

            ViewControllerBase retVal = PopViewControllerLocal();
            if (retVal != null)
            {
                retVal.unloadView();
            }

            if (viewControllersStack.Count == 0)
            {
                SetViewControllerVisible(rootViewController, true);
            }
            else
            {
                topViewController = viewControllersStack.Peek();
                SetViewControllerVisible(topViewController, true);
            }
            return retVal;
        }
        return null;
    }

    public static void PopViewToRoot()
    {
        UIManager.sharedInstance.PopToRootController();
    }

    private void PopToRootController()
    {
        if (viewControllersStack.Count > 0)
        {
            do
            {
                ViewControllerBase retVal = PopViewControllerLocal();
                if (retVal != null)
                {
                    SetViewControllerVisible(retVal, false);
                    retVal.unloadView();
                }
            }
            while (viewControllersStack.Count > 0);
            SetViewControllerVisible(rootViewController, true);
        }
    }

    private void SetViewControllerVisible(ViewControllerBase viewController, bool isVisible)
    {
        if (isVisible)
        {
            viewController.viewWillAppear();
            viewController.viewDidAppear();
        }
        else
        {
            viewController.viewWillDisAppear();
            viewController.viewDidDisAppear();
        }
    }

    public static void PushViewAsRoot(ID_POPUP item)
    {
        UIManager.sharedInstance.PushViewControllerAsRoot(item);
    }

    private ViewControllerBase PushViewControllerAsRoot(ID_POPUP item)
    {
        if (viewControllersStack.Count > 0)
        {
            do
            {
                ViewControllerBase retVal = PopViewControllerLocal();
                if (retVal != null)
                {
                    SetViewControllerVisible(retVal, false);
                    retVal.unloadView();
                }
            }
            while (viewControllersStack.Count > 0);
        }
        SetViewControllerVisible(rootViewController, false);
        rootViewController.unloadView();
        rootViewController = InstantiateObject(item);
        SetViewControllerVisible(rootViewController, true);
        return rootViewController;
    }

    public static ViewControllerBase PushView(ID_POPUP item)
    {
        return UIManager.sharedInstance.PushViewController(item);
    }

    private ViewControllerBase PushViewController(ID_POPUP item)
    {
        if (viewControllersStack.Count != 0)
        {
            SetViewControllerVisible(topViewController, false);
        }
        else
        {
            SetViewControllerVisible(rootViewController, false);
        }
        ViewControllerBase viewController = InstantiateObject(item);
        SetViewControllerVisible(viewController, true);
        viewControllersStack.Push(viewController);
        topViewController = viewController;
        return viewController;
    }

    public static ViewControllerBase PushViewPopUp(ID_POPUP item)
    {
        return UIManager.sharedInstance.PushViewControllerPopup(item);
    }

    private ViewControllerBase PushViewControllerPopup(ID_POPUP item)
    {
        if (viewControllersStack.Count != 0)
        {
            topViewController.viewWillDisAppear();
        }
        else
        {
            rootViewController.viewWillDisAppear();
        }

        ViewControllerBase viewController = InstantiateObject(item);
        SetViewControllerVisible(viewController, true);
        viewControllersStack.Push(viewController);
        topViewController = viewController;
        return viewController;
    }

    public bool IsRootViewControllerActive()
    {
        return (viewControllersStack.Count == 0);
    }

    public bool CheckTopType(ID_POPUP type)
    {
        if (viewControllersStack.Count > 0)
        {
            return type == topViewController.ID;
        }
        else
        {
            return type == rootViewController.ID;
        }
    }

    public ViewControllerBase GetTopView()
    {
        if (viewControllersStack.Count > 0)
        {
            return topViewController;
        }
        else
        {
            return rootViewController;
        }
    }

    public void ShowLoadingPanel()
    {
        loadingPanel.gameObject.SetActive(true);
    }

    public void HideLoadingPanel()
    {
        loadingPanel.gameObject.SetActive(false);
    }
}
