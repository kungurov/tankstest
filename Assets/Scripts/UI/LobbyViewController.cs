﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;

public class LobbyViewController : ViewControllerBase
{
    private LobbyManager lobbyManager;

    public GameObject lobbyPlayerView;
    public UIGrid grid;
    public UIScrollView scrollView;
    public UIButton btnBack;

    public override void onStart()
    {
        lobbyManager = LobbyManager.Instance;
    }

    public override void viewWillAppear()
    {
        base.viewWillAppear();
        btnBack.isEnabled = true;

        UIEventListener.Get(btnBack.gameObject).onClick += OnBackClick;
    }

    public override void viewWillDisAppear()
    {
        base.viewWillDisAppear();
        btnBack.isEnabled = false;

        UIEventListener.Get(btnBack.gameObject).onClick -= OnBackClick;
    }

    void OnBackClick(GameObject sender)
    {
        lobbyManager.StopMatchMaker();
        UIManager.PopViewToRoot();
    }

    public LobbyPlayerView CreateLobbyPlayerView()
    {
        LobbyPlayerView view = NGUITools.AddChild(grid.gameObject, lobbyPlayerView).GetComponent<LobbyPlayerView>();
        grid.Reposition();
        scrollView.ResetPosition();
        return view;
    }

    public void DestroyLobbyPlayerView(LobbyPlayerView lobbyPlayerView)
    {
        NGUITools.Destroy(lobbyPlayerView.gameObject);
        grid.Reposition();
        scrollView.ResetPosition();
    }
}
