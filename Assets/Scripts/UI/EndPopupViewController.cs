﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndPopupViewController : ViewControllerBase 
{
	[SerializeField]
	private UILabel labelStatus;

	public void ShowEndGameStatus(END_GAME_STATUS status)
	{
		switch (status) 
		{
		case END_GAME_STATUS.TIME_OUT:
			labelStatus.text = "Время раунда вышло";
			break;
		case END_GAME_STATUS.RED_WIN:
			labelStatus.text = "Победили красные";
			labelStatus.color = Color.red;
			break;
		case END_GAME_STATUS.BLUE_WIN:
			labelStatus.text = "Победили синие";
			labelStatus.color = Color.blue;
			break;
		}
	}
}
