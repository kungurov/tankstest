﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;

public class FindServersViewController : ViewControllerBase
{
    private LobbyManager lobbyManager;

    public GameObject serverItem;
    public UIGrid grid;
    public UIScrollView scrollView;
    public UILabel labelStatus;

    public UIButton btnBack;
    public UIButton btnRefresh;

    public override void onStart()
    {
        lobbyManager = LobbyManager.Instance;
        lobbyManager.StartMatchMaker();
        RequestPage();
    }

    public override void viewWillAppear()
    {
        base.viewWillAppear();
        btnBack.isEnabled = true;
        btnRefresh.isEnabled = true;

        UIEventListener.Get(btnBack.gameObject).onClick += OnBackClick;
        UIEventListener.Get(btnRefresh.gameObject).onClick += OnRefreshClick;
    }

    public override void viewWillDisAppear()
    {
        base.viewWillDisAppear();
        btnBack.isEnabled = false;
        btnRefresh.isEnabled = false;

        UIEventListener.Get(btnBack.gameObject).onClick -= OnBackClick;
        UIEventListener.Get(btnRefresh.gameObject).onClick -= OnRefreshClick;
    }

    void OnBackClick(GameObject sender)
    {
        lobbyManager.StopMatchMaker();
        UIManager.PopView();
    }

    void OnRefreshClick(GameObject sender)
    {
        HideStatusLabel();
        ClearServerItems();
        RequestPage();
    }

    public void RequestPage()
    {
        UIManager.sharedInstance.ShowLoadingPanel();
        lobbyManager.matchMaker.ListMatches(0, 10, "", true, 0, 0, OnGUIMatchList);
    }

    public void OnGUIMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matches)
    {
        UIManager.sharedInstance.HideLoadingPanel();
        if (success)
        {
            if (matches.Count == 0)
            {
                ShowStatusLabel("Нет доступных серверов");
            }
            else
            {
                ShowServerItems(matches);
            }
        }
        else
        {
            ShowStatusLabel("Ошибка");
        }
    }

    private void ShowStatusLabel(string text)
    {
        labelStatus.gameObject.SetActive(true);
        labelStatus.text = text;
    }

    private void HideStatusLabel()
    {
        labelStatus.gameObject.SetActive(false);
    }

    private void ShowServerItems(List<MatchInfoSnapshot> matches)
    {
        if (matches != null)
        {
            for (int i = 0; i < matches.Count; i++)
            {
                ServerItemView item = NGUITools.AddChild(grid.gameObject, serverItem).GetComponent<ServerItemView>();
                item.SetServerData(matches[i]);
            }
            grid.Reposition();
            scrollView.ResetPosition();
        }
    }

    private void ClearServerItems()
    {
        foreach (Transform child in grid.GetChildList())
        {
            NGUITools.Destroy(child.gameObject);
        }
    }
}
